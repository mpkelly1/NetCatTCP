/**
 *  Author: Michael Kelly
 *  Date: Spring 2017
 *  Title:  NetcatClient TCP
 */

package csci4311.nc;

import java.net.*;
import java.util.Scanner;
import java.io.*;

public class NetCatClientTCP {
  private static Socket socket;
  private static BufferedReader in;

  public static void main(String[] args) throws Exception {
    run(args[0], Integer.parseInt(args[1]));
  }

  private static void run(String host, int port) throws Exception {
    socket = new Socket(host, port);
    if (System.in.available() > 0){
      runUpload();
    } else {
      runDownload();
    }

    socket.close();
  }

  private static void runUpload() throws Exception {
    DataOutputStream out = new DataOutputStream(socket.getOutputStream());
    out.writeBytes(new Scanner(System.in).useDelimiter("\\Z").next());
  }

  private static void runDownload() throws Exception {
    in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    String input;
    while ((input = in.readLine()) != null ){
      System.out.println(input);
    }
  }
}
