/**
 *  Author: Michael Kelly
 *  Date: Spring 2017
 *  Title:  NetCatServer TCP side
 */

package csci4311.nc;

import java.net.*;
import java.util.Scanner;
import java.io.*;

public class NetCatServerTCP 
{
  private static ServerSocket welcomeSocket;
  private static Socket connectionSocket;
  private static BufferedReader in;

  public static void main(String[] args) throws Exception {
    run(Integer.parseInt(args[0]));
  }

  private static void run(int port) throws Exception 
  {
    connectionSocket = null;
    welcomeSocket = new ServerSocket(port , 0);
  	System.out.println("Server Ready for Connection");
    boolean success = false;
  	// While loop to handle arbitrary sequence of clients making requests
  	while(true) 
    {
		// Wait for some client to connect and create new socket for connection
  		if(connectionSocket == null) 
      {
  			connectionSocket = welcomeSocket.accept();
  			System.out.println( "Client Made Connection");
  		} else 
      {
        if (System.in.available() > 0){
          System.out.println("Running download");
          success = runDownload();
        } else 
        {
          System.out.println("Running upload");
          success = runUpload();
        }
      }

      if (success){
        connectionSocket.close();
        break;
      }
    }

  }

  private static boolean runDownload() throws Exception {
    DataOutputStream out = new DataOutputStream(connectionSocket.getOutputStream());
    out.writeBytes(new Scanner(System.in).useDelimiter("\\Z").next());
    return true;
  }

  private static boolean runUpload() throws Exception{
    if (in == null){
      in = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
    }

    String input;
    while ((input = in.readLine()) != null ){
      System.out.println(input);
    }
    return true;
  }
}
