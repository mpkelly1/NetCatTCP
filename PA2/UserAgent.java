
package csci4311.chat;

public interface UserAgent {
    // Deliver a (remote) message to the user agent
    public void deliver(MsgpMessage message);
}