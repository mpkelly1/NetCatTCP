
/**
 *	Author: Michael Kelly
 *	Date: April 15, 2017
 *	Title:  CLI User Agent that implements UserAgent interface
 *	-->Main class of Client side of chat program
 *	-->Parses command line inputs for chat prog
 */

package csci4311.chat;

import java.util.*;
import java.io.*;
import java.net.*;

public class CLIUserAgent implements UserAgent
{
	// Client socket variable
	private Socket clientSocket;
	// TextMsgpClient variable 
	private TextMsgpClient msgp;
	// Port Number
	private int port;	
	// Recepient List of sting array 
	private ArrayList<String> recipients;
	//  Outgoing Message
	private String messageOut;
	// User Name
	private String userName;

	
	
	public CLIUserAgent (String userName, String server, int port) throws Exception
	{
		//Instantiate username
		this.userName = userName;
		//Instantiate a blank message to avoid null ref.
		messageOut= "";
		//instantiate the recepient array
		recipients = new ArrayList<String>();
		
		// Create socket to accept @param port
		clientSocket = new Socket(server,port);
		System.out.println("Connection established: User: "+userName+" & Server: "+server+".");

		// Instantiate a new message client with the client socket
		msgp = new TextMsgpClient( this, clientSocket );  
		
		//Join request will record user's name automatically after login
		msgp.join(userName,"ReservedGroup");
		//Leave the group action
		msgp.leave(userName,"ReservedGroup");

		// Instantiate scanner for user's message input
		Scanner input = new Scanner(System.in);
		String command = "";
		
		//Create string array for the parsed commands
		String[] commandParts = null; 
		// The reply code received as a response of requests
		int replyCode = 0;

		
		// Infinite loop to read/send user's messages
		while(true)
		{
			// Print the prompt
			System.out.print("\n@"+userName+" >>");
			
			// Parse user input
			command = input.nextLine();
			
			// If the user does not specify a "send" command....
			if ( !command.startsWith("send") )
			{
				//Parse the command with spaces between parameters
				commandParts = command.split(" ");
			
				// If it's a "join" command....
				if ( commandParts[0].equals("join") )
				{
					//Pass the username and group-to-join as parameters
					//Return variable will be a reply code integer
					replyCode = msgp.join( userName, commandParts[1] );

					//If the user is already a member of a group (code 201)
					if ( replyCode == 201 )
						//Print out to user group membership already exists
						System.out.println("Reply 201: User already belongs to a group.");
					//If in the event that user commands a "send"
					else
					{
						//Call to msgp method to return the userlist
						 ArrayList<String> users = (ArrayList<String>)msgp.users(commandParts[1]);
						 // Check if users group is not empty, set it to the user group size()
						 int userCount = 0;
						 if(users != null)
						 {
						 	userCount = users.size();
						 }						
					
						 // Report to user the group was joined and display number of members in group.
						System.out.println("Joined #"+commandParts[1]+" with "+userCount+" current members.");					 
					}
						

				}

				// If the user cmd line is "leave"
				else if (commandParts[0].equals("leave"))
				{
					if(commandParts.length > 1)
					{
						//Invoke msgp method to leave, return: replyCode
						replyCode = msgp.leave(userName, commandParts[1]);
					}

					//Switch statement for reply codes...
					switch (replyCode)
					{
						case 200:  //For successful action, no output
									break;
						case 201:
									System.out.println("Not a member of #" +commandParts[1]);
									break;
						case 400:
									System.out.println("Error 400: "+commandParts[1]+" not a valid group.");
									break;
						default: System.out.println("Command not recognized.");
					}
				}

				//If user command is "groups"
				else if (commandParts[0].equals("groups"))
				{
					
					//Invoke msgp method to produce array-list of groups
					ArrayList<String> groups = (ArrayList<String>)msgp.groups();

					//Declare Array List to count the # of users in a group
					ArrayList<String> users;
					
					//If Array List of groups is not null
						// Iterate through each group and count users
					if (groups != null)
					{
						for (String u: groups)
						{
							
							//Invoke msgp method users() to return array of user names
							users = (ArrayList<String>)msgp.users(u);
							//Count the number of users 
							int userCount = 0;
							if(users != null)
							{
								userCount = users.size();
							}

							// display the group's name and its number of members
							System.out.println("#"+ u +" has "+userCount+" members");
						}
					}
				}

				//If user cmd line is "users"
				else if (commandParts[0].equals("users"))
				{
					if(commandParts.length > 1)
					{
							//Invoke msgp method for users() to create a string of users
								//Must pass-in  a specified group
							ArrayList<String> users = (ArrayList<String>)msgp.users(commandParts[1]);
							for (String u: users)
						{
							System.out.println("@"+u);
						}
					}
				}

				//If user cmdline specifies "history"
				else if (commandParts[0].equals("history"))
				{
					if(commandParts.length > 1)
					{
						//Invoke msgp method for history() to return an array list of past-messages
							//Must pass in a specified group
						ArrayList<MsgpMessage> history = (ArrayList<MsgpMessage>)msgp.history( commandParts[1]);
						//Use embedded method to print the enumerated list of messages
						this.printHistory(history);
					}

				}
					
			}

			// ELSE: "send"	command
			else
			{
				// calls helper method to extract the recipient list and the message from the send command
				//Calls method extractSendCommand to pull recipient list and 
					// pull the message from the send cmd
				extractSendCommand(command);

				// Generate reply code for succesful send
				replyCode = msgp.send(new MsgpMessage(userName,recipients,messageOut));
				if (replyCode != 200)
				{
					System.out.println("User(s) not found");
				}
			}
			
		}//End of while loop
	}


	// Main method
	public static void main(String args[]) throws Exception
	{
		// If number of cmd line args too few/many report to user and exit
		if (args.length < 1)
		{
			System.out.println("Missing arguments from command-line.");
			System.exit(0);
		if (args.length > 3)
			System.out.println("Too many arguments in command-line.");
			System.exit(0);
		}
		
		// Instantiate variables from user's cmd line parsing
		String userName = args[0];
		//Server name default "localhost" unless user-specified
		String server = args.length > 1 ? args[1] : "localhost";
		// Set default port to 4311 unless user-specified 
		int port = args.length == 3 ? Integer.parseInt(args[2]) : 4311 ;
		
		// Declare/Instantiate new CLIUseragent with variables above
			// param: userName, server, and port
		CLIUserAgent userAgent = new CLIUserAgent(userName, server, port);
	}//End of main


	//Helper method to print the message history of a specified group
		//@param arraylist object created by msgp method history()
	public void printHistory(ArrayList<MsgpMessage> history)
	{
		for (MsgpMessage message : history )
		{
			System.out.println("["+message.getFrom()+"] "+message.getMessage());
		} 	
	}



	//Method to seperate the "send" cmd from the actual user message
		//@param String of the entire command line
	private void extractSendCommand(String command)
	{
		//Must clear the recepient list from last usage
		recipients.clear();
		
		//Split the String cmd line into two parts delineated by a space n-1 times
		String subStrings[] = command.split(" ",2);
		
		do 
		{
			// split the second subString in two by n-1 times
			subStrings = subStrings[1].split(" ",2);
			//the first subString should be a recipient, add it to the list
			recipients.add( subStrings[0] );
		}
		// continue until there is more recipient to extract
		while( subStrings[1].startsWith("@") || subStrings[1].startsWith("#") );

		// SubString without "@" or "#" is the message
		messageOut = subStrings[1];
		
	}// end of method extractSendCommand


	//deliver() method to deliver incoming message to specified user
		//@param of MsgpMessage encoded message
	public void deliver(MsgpMessage message)
	{
		System.out.println("["+message.getFrom()+"] "+message.getMessage() );

		// if not a message received from self, reprint the prompt
		if ( !message.getFrom().equals( userName ) )
			System.out.print("\n@"+userName+" >>");
	}

	


}// EOF	