/**
 *  Author: Michael Kelly
 *  Date: April 16, 2017
 *  Title:  Group class
 *  -->Group object class
 * 	-->Handles name of group + list of members
 *	-->Also records history of messages
 */

package csci4311.chat;

import java.util.*;

public class Group
{	
	// the name of the group
	private String name;
	// the ArrayList that saves the group's members' names
	private ArrayList<String> members;
	// the LinkedList that saves the group's chat history
	private ArrayList<String> history;

	
	// Constructor
		//@param: string name for the group
	public Group(String name)
	{	
		this.name = name; 
		members = new ArrayList<String>();
		history = new ArrayList<String>();
	}


	//Getter/Setter methods

	//Get members list
	public ArrayList<String> getMembers()
	{
		return this.members;
	}

	//Get size of group (# of members)
	public int getSize()
	{
		return this.members.size();
	}
		// Get group's name
	public String getName()
	{
		return this.name;
	}
	
	//Get Group History of messages
	public ArrayList<String> getHistory()
	{
		return this.history;
	}
	
	//Check for user membership
	public boolean contains( String user )
	{
		return members.contains( user );
	}

	//Add user to group
	public void addMember ( String user )
	{
		members.add( user );
	} 

	//Remove user from group
	public void removeMember ( String user )
	{
		members.remove( user );
	}

	//Add message to history
	public void addHistory( String message )
	{
		history.add( message );
	}

	//Clear group history of messages
	public void clearHistory( String message )
	{
		history.clear();
	}
}