/**
 *  Author: Michael Kelly
 *  Date: April 15, 2017
 *  Title:  TextMsgpClient implements the msgp protocols
 *  -->Parses command line inputs for chat prog
 */

package csci4311.chat;

import java.util.*;
import java.io.*;
import java.net.*;

public class TextMsgpClient extends Thread implements MsgpClient
 {	
 	
 	
 	//clientRequest to be sent to server
 	private String clientRequest;
 	//String variable to store server's serverReply
 	private String serverReply;
 	//Tempo storage of server serverReply
 	private String serverReplyBuffer;
 	//Var for userAgent that invokes this
 	private CLIUserAgent userAgent;
    //DataOutputStream for outputting messages
    private DataOutputStream outputStream;
    //DataInputStream for the server communications
    private DataInputStream inputStream;
 	
 	// constructor
 	public TextMsgpClient(CLIUserAgent userAgent,Socket clientSocket) throws Exception
 	{
        //Instantiate output stream
 		this.outputStream = new DataOutputStream( clientSocket.getOutputStream());
        //Input Stream 
 		this.inputStream = new DataInputStream( clientSocket.getInputStream());
        //To avoid null, instantiate string vars to empty
 		this.clientRequest = "";
 		this.serverReply = "";
 		this.userAgent = userAgent;
        //Build thread to handle incoming messages
		this.start();
 	}

    //join() method for adding specified user to a specificied group
        //@param: String of the username, String of the group name
        //returns integer of the replyCode
    public int join(String user, String group)
    {
    	clientRequest = "msgp join "+user+" "+group;
    	try
    	{
    		outputStream.writeUTF(clientRequest);

			this.getserverReply();
    	}
    	catch (IOException e)
    	{
    		e.printStackTrace();
    	}
        // Parse the server reply for the reply code
    	finally
    	{
    		return Integer.parseInt( serverReply.split(" ",3)[1] );
    	}
    }

    //leave() method for removing a user from a specific group
        //@param string user and string group
        //return:  integer reply code
    public int leave(String user, String group)
    {
    	clientRequest = "msgp leave "+user+" "+group;
    	try
    	{
    		outputStream.writeUTF(clientRequest);
		
    		this.getserverReply();
    	}
    	catch (IOException e)
    	{
    		e.printStackTrace();
    	}
    	finally
    	{
            //parse the server reply for the reply code
    		return Integer.parseInt( serverReply.split(" ",3)[1] );
    	}
	}

    //groups() to return the group names and number of members
        //@param: none
        //returns a List of String variables.
    public List<String> groups()
    {
    	ArrayList<String> groups = null;
    	clientRequest = "msgp groups";
    	try
    	{
    		outputStream.writeUTF(clientRequest);
		
    		this.getserverReply();
    	}
    	catch (IOException e)
    	{
    		e.printStackTrace();
    	}
    	finally
    	{
            //use string method startsWith to parse server reply
                //if not "msgp 201" -> no result, 
    		if ( !serverReply.startsWith("msgp 201") )
    		{
    			// split the serverReply into group names using delimiter "\n" and save them to the array list 
				groups = new ArrayList<String>( Arrays.asList(serverReply.split("\n")));

				// remove the first string in the list, which is the protocol's response message
				groups.remove(0);
			}
  			return groups;
    	}
    }

    //users() for returning list of users in a specified group
        //@param: String group to report user list
        //return a List of strings of users
    public List<String> users(String group)
    {
    	ArrayList<String> users = null;
    	clientRequest = "msgp users "+group;
    	try
    	{
    		outputStream.writeUTF(clientRequest);
		
    		this.getserverReply();
    	}
    	catch (IOException e)
    	{
    		e.printStackTrace();
    	}
    	finally
    	{
            //If server reply code is 'successful'
    		if (serverReply.startsWith("msgp 200"))
    		{
    			// split the serverReply into user names using delimiter "\n" 
                // save them to the array list
				users = new ArrayList<String>(Arrays.asList(serverReply.split("\n")));
				// remove the first string in the list, which is the protocol's response message
				users.remove(0);
			}

    		return users;
    	}
    }

    /**
     * clientRequests the history of chat message of a group.
     *
     * @param   group   group name
     * @return          list of all messages sent to the group; null of none
     */
    public List<MsgpMessage> history(String group)
    {
    	List<MsgpMessage> history = new ArrayList<MsgpMessage>();
    	clientRequest = "msgp history "+group;
    	try
    	{
    		outputStream.writeUTF(clientRequest);
		
    		this.getserverReply();

    	}
    	catch (IOException e)
    	{
    		e.printStackTrace();
    	}
    	finally
    	{
    		// prints serverReply message in cases of No result and Error
    		//if ( serverReply.startsWith("msgp 201") || serverReply.startsWith("msgp 400") )
    			//System.out.println( serverReply );

    		// valid case
    		if ( serverReply.startsWith("msgp 200") )
    		{
    			// split the history into individual messages
				// also get rid of the not-needed header "msgp send" by including it in the split delimiter
				String[] messages = serverReply.split("\n\nmsgp send");

				// decode all messages in the list and save them to the ArrayList history
				for ( String m: messages )
				{
					history.add( decodeMessage( m ) );
				}
    		}
			
			
    		return history;
    	}
    }


    /**
     * Encodes the sending of a message.
     *
     * @param message   message content
     * @return serverReply code, as per the spec
     */

    public int send(MsgpMessage msg)
    {
    	clientRequest = "msgp send\nfrom: "+msg.getFrom()+"\n";
    	for ( String t: msg.getTo())
    	{
    		clientRequest += ("to: "+ t +"\n");
    	}
    	clientRequest += "\n"+msg.getMessage()+"\n\n";
    	
    	try
    	{
    		outputStream.writeUTF(clientRequest);
		
    		this.getserverReply();
    	}
    	catch (IOException e)
    	{
    		e.printStackTrace();
    	}
    	finally
    	{
    		//System.out.println(serverReply);
    		return Integer.parseInt( serverReply.split(" ",3)[1] );
    	} 
    }

    

	/**
	 * helper method 
	 * gets response message from the listenner thread
	 * saves it to instance variable serverReply
	 **/
    private void getserverReply()
    {
    	do
		{
			try
			{
				Thread.sleep(25);
			}
			catch( InterruptedException ie)
			{
				ie.printStackTrace();
			}
		}
		while( serverReplyBuffer.equals("") );
		
		serverReply = serverReplyBuffer;
		serverReplyBuffer = "";
    }


	/**
	 * helper method
	 * decodes an incoming message
	 * 
	 * @param msd 	the message to be decoded
	 * @return 		the MsgpMessage object that represents the decoded message 
	 **/
    private MsgpMessage decodeMessage(String msg)
    {
        // chain of msgp-specific procecures to decode a message into sender and message:

        // get rid of the header, get straight to the sender line
        msg = msg.substring( msg.indexOf("from") );

        // split the above string in two by delimiter "\n"
        // the first part is the sender line, save this
		String sender = msg.split("\n",2)[0];

		// split the sender line by delimiter " "
		// the second part is the sender, save this
		sender = sender.split(" ")[1];

		// split the message in two by delimiter "\n\n"
		// the second part is the message itself, save this
		String message = msg.split("\n\n",2)[1];

		// the message part may still contain the trailing new-line character
		// this is not needed, get rid of it
		if ( message.contains("\n") )
			message = message.substring(0,message.indexOf("\n"));

		// return the message in a MsgpMessage object, recipients are not needed here
		return new MsgpMessage(sender,null,message);
    }


	
    /** 
     * the message-listenner thread run this method
     * constantly listens to the input stream for messages and replies
     **/
	public void run()
	{
		// the incoming message
 		String messageIn;
 		
		try
		{
			while( true )
			{
				messageIn = inputStream.readUTF();

				// if receives a message, send it to user agent to inputStreamplay to the user
				if (messageIn.startsWith("msgp send"))
				{
					userAgent.deliver( decodeMessage( messageIn ) );
				}
				
				// otherwise, its a serverReply 
				// save it to the serverReplyBuffer to be retrieved by whom it concerns 
				else
				{
					serverReplyBuffer = messageIn;
				}
			}
		}
		catch ( IOException e )
		{
			e.printStackTrace();
		}	
	} // end of run method
	
 }// end of class
