/* Networking 4311-S17
* PA2
* Due: April 19, 2017 11:59pm
*/

Program Invocation
Client:
    java csci4311.chat.CLIUserAgent <user> <server> [<port>]
Server:
    java csci4311.chat.ChatServer [<port>]
    
The default port (if not specified) is 4311.